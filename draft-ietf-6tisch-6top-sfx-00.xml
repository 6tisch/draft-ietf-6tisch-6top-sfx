﻿<?xml version="1.0"?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd">
<?rfc toc="yes"?>
<?rfc comments="yes"?3
<?rfc inline="yes"?>
<?rfc subcompact="yes"?>
<rfc category="exp" ipr="trust200902" docName="draft-ietf-6tisch-6top-sfx-00">
<front>
    <title abbrev="6tisch-6top-sfx">
        6TiSCH 6top Scheduling Function Zero / Experimental (SFX)
    </title>
    <author initials="D" surname="Dujovne" fullname="Diego Dujovne" role="editor">
        <organization>Universidad Diego Portales</organization>
        <address>
            <postal>
                <street>Escuela de Informatica y Telecomunicaciones</street>
                <street>Av. Ejercito 441</street>
                <city>Santiago</city>
                <region>Region Metropolitana</region>
                <country>Chile</country>
            </postal>
            <phone>+56 (2) 676-8121</phone>
            <email>diego.dujovne@mail.udp.cl</email>
        </address>
    </author>
    <author initials="LA" surname="Grieco" fullname="Luigi Alfredo Grieco">
        <organization>Politecnico di Bari</organization>
        <address>
            <postal>
                <street>Department of Electrical and Information Engineering</street>
                <street>Via Orabona 4</street>
                <city>Bari</city>
                <code>70125</code>
                <country>Italy</country>
            </postal>
            <phone>00390805963911</phone>
            <email>a.grieco@poliba.it</email>
        </address>
    </author>
    <author initials="MR" surname="Palattella" fullname="Maria Rita Palattella">
        <organization>Luxembourg Institute of Science and Technology (LIST)</organization>
        <address>
            <postal>
                <street>Department 'Environmental Research and Innovation' (ERIN)</street>
                <street>41, rue du Brill</street>
                <city>Belvaux</city>
                <code>L-4422</code>
                <country>Grand-duchy of Luxembourg</country>
            </postal>
            <phone>+352 275 888-5055</phone>
            <email>mariarita.palattella@list.lu</email>
        </address>
    </author>
    <author initials="N" surname="Accettura" fullname="Nicola Accettura">
        <organization>LAAS-CNRS</organization>
        <address>
            <postal>
                <street>7, avenue du Colonel Roche</street>
                <city>Toulouse</city>
                <code>31400</code>
                <country>France</country>
            </postal>
            <phone>+33 5 61 33 69 76</phone>
            <email>nicola.accettura@laas.fr</email>
        </address>
    </author>
    <date/>
    <area>Internet Area</area>
    <workgroup>6TiSCH</workgroup>
    <keyword>Draft</keyword>
    <abstract>
        <t>
            This document defines a Scheduling Function called "Scheduling Function Zero" (SF0) in its Experimental version.
            SF0 dynamically adapts the number of scheduled cells between neighbor nodes, based on the amount of currently allocated cells and the neighbor nodes' cell requirements.
            Neighbor nodes negotiate in a distributed neighbor-to-neighbor basis the number of cell(s) to be added/deleted.
            SF0 uses the 6P signaling messages to add/delete cells in the schedule.
            This function selects the candidate cells from the schedule, defines which cells will be added/deleted and triggers the allocation/deallocation process.
        </t>
    </abstract>
    <note title="Requirements Language">
        <t>
            The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in <xref target="RFC2119">RFC 2119</xref>.
        </t>
    </note>
</front>
<middle>
    <section title="TEMPORARY EDITORIAL NOTES">
        <t>
            This document is an Internet Draft, so it is work-in-progress by nature.
            It contains the following work-in-progress elements:
            <list style="symbols">
                <t>
                    "TODO" statements are elements which have not yet been written by the authors for some reason (lack of time, ongoing discussions with no clear consensus, etc).
                    The statement does indicate that the text will be written at some time.
                </t>
                <t>
                    "TEMPORARY" appendices are there to capture current ongoing discussions, or the changelog of the document.
                    These appendices will be removed in the final text.
                </t>
                <t>
                    "IANA_" identifiers are placeholders for numbers assigned by IANA.
                    These placeholders are to be replaced by the actual values they represent after their assignment by IANA.
                </t>
                <t>
                    The string "REMARK" is put before a remark (questions, suggestion, etc) from an author, editor of contributor.
                    These are on-going discussions at the time to writing, NOT part of the final text.
                </t>
                <t>
                    This section will be removed in the final text.
                </t>
            </list>
        </t>
    </section>
    <section title="Introduction"                                    anchor="sec_intro">
        <t>
            This document defines a minimal Scheduling Function using the 6P protocol <xref target="I-D.ietf-6tisch-6top-protocol"/>, called "Scheduling Function Zero" (SF0).
            SF0 is designed to offer a number of functionalities to be usable in a wide range of applications.
            SF0 defines two algorithms: The Scheduling Algorithm defines the number of cells to allocate/delete between two neighbours and the Relocation Algorithm defines when to relocate a cell.
        </t>
        <t>
           To synthesize, a node running SF0 determines when to add/delete cells in a three-step process:
        </t>
        <t>
            <list style="numbers">
                <t>
                    It waits for a triggering event (<xref target="sec_triggers"/>).
                </t>
                <t>
                    It applies the Cell Estimation Algorithm (CEA) for a particular neighbor to determine how many cells are required to that neighbor (<xref target="sec_bw_estimation"/>).
                </t>
                <t>
                    It applies the Allocation Policy to compare the number of required cells to the number of already scheduled cells, and determines the number of cells to add/delete (<xref target="sec_allocation_policy"/>).
                </t>
            </list>
        </t>
        <t>
            We expect additional SFs, offering more functionalities for a more specific use case, to be defined in future documents.
            SF0 addresses the requirements for a scheduling function listed in Section 5.2 from <xref target="I-D.ietf-6tisch-6top-protocol"/>, and follows the recommended outline listed in Section 5.3 of <xref target="I-D.ietf-6tisch-6top-protocol"/>.
            This document follows the terminology defined in <xref target="I-D.ietf-6tisch-terminology"/>.
        </t>
    </section>
    <section title="Scheduling Function Identifier"                  anchor="sec_sfid">
        <t>
            The Scheduling Function Identifier (SFID) of SF0 is IANA_6TISCH_SFID_SF0.
        </t>
    </section>
    <section title="Allocated and Used Cells"                  anchor="sec_allocatedandusedcells">
        <t>
            An allocated cell is assigned as a TX, RX or Shared cell on the schedule, as a reserved resource.
            This reservation does not imply that a packet will be transmitted during the scheduled cell time.
            A used cell is a cell where a packet has been transmitted during the scheduled cell time on the last slotframe.
        </t>
    </section>
    <section title="Overprovisioning"                  anchor="sec_overprovisioning">
        <t>
            Overprovisioning is the action and effect of increasing a value representing an amount of resources.
            In the case of SF0, overprovisioning is done as a provision to reduce traffic variability effects on packet loss, to the expense of artificially allocating a number of cells.
        </t>
    </section>
    <section title="Scheduling Algorithm"                  anchor="sec_schedulingalgorithm">
        <t>
            A number of TX cells must be allocated between neighbor nodes in order to enable data transmission among them.
            A portion of these allocated cells will be used by neighbors, while the remaining cells can be over-provisioned to handle unanticipated increases in cell requirements.
            The Scheduling Algorithm collects the cell allocation/deallocation requests from the neighbors and the number of cells which are currently under usage.
            First, the Cell Estimation Algorithm calculates the number of required cells and second, the calculated number is transferred to the Allocation Policy.
            In order to reduce consumption, this algorithm is triggered only when there is a change on the number of used cells from a particular node.
        </t>
        <section title="SF0 Triggering Events"                       anchor="sec_triggers">
            <t>
                We RECOMMEND SF0 to be triggered at by the following event: If there is a change on the number of used cells towards any of the neighbours.
                The exact mechanism of when SF0 is triggered is implementation-specific.
            </t>
        </section>
        <section title="SF0 Cell Estimation Algorithm"          anchor="sec_bw_estimation">
            <t>
                The Cell Estimation Algorithm takes into account the number of current used cells to the neighbour.
                This allows the algorithm to estimate a new number of cells to be scheduled to the neighbour.
                As a consequence, the Cell Estimation Algorithm for SF0 follows the steps described below:
                <list style="numbers">
                    <t>Collect the current number of used cells to the neighbour</t>
                    <t>Calculate the new number of cells to be scheduled to the neighbour by adding the current number of used cells plus an OVERPROVISION number of cells</t>
                    <t>Transfer the request to the allocation policy as REQUIREDCELLS</t>
                    <t>Return to step 1 and wait for a triggering event.</t>
                </list>
                The Cell Estimation Algorithm is depicted on figure <xref target="fig_estimationalgorithm" />.
                The OVERPROVISION parameter is calculated as a percentage of the number of currently scheduled cells to the neighbour.
                OVERPROVISION is added to the amount of used cells to the neighbour to reduce the probability of packet loss given a sudden growth on the number of used cells to the neighbour.
                The OVERPROVISION value is implementation-specific.
                A value of OVERPROVISION equal to zero leads to queue growth and possible packet loss: In this case, there are no overprovisioned cells where a sudden growth on the number of cells can absorbed and detected.
            </t>
            <t>
                <figure title="The SF0 Estimation Algorithm"            anchor="fig_estimationalgorithm">
<artwork><![CDATA[
   +-------------------+
   |   Triggering      |
   |   Event           |<-----+
   |                   |      |
   +-------------------+      |
             |                |
             V                |
   +-------------------+      |
   | Collect number of |      |
   | used cells        |      |
   +-------------------+      |
             |                |
             V                |
   +-------------------+      |
   |   used cells      |      |
   |       +           |      |
   |  OVERPROVISION    |      |
   |       =           |      |
   |  REQUIREDCELLS    |      |
   +-------------------+      |
             |                |
             V                |
   +-------------------+      |
   |  REQUIREDCELLS    |      |
   |         |         |      |
   |         V         |------+
   |  Allocation       |
   |  Policy           |
   +-------------------+
               ]]></artwork>
             </figure>
            </t>
        </section>
        <section title="SF0 Allocation Policy"                       anchor="sec_allocation_policy">
            <t>
                The "Allocation Policy" is the set of rules used by SF0 to decide when to add/delete cells to a particular neighbor to satisfy the cell requirements.
            </t>
            <t>
                SF0 uses the following parameters:
            </t>
            <t>
                <list hangIndent="3" style="hanging">
                    <t hangText="SCHEDULEDCELLS:">
                        The number of cells scheduled from the current node to a particular neighbor.
                    </t>
                    <t hangText="REQUIREDCELLS:">
                        The number of cells calculated by the Cell Estimation Algorithm from the current node to that neighbor.
                    </t>
                    <t hangText="SF0THRESH:">
                        Threshold parameter introducing cell over-provisioning in the allocation policy.
                        It is a non-negative value expressed as number of cells.
                        The definition of this value is implementation-specific.
                        A setting of SF0THRESH>0 will cause the node to allocate at least SF0THRESH cells to each of its' neighbors.
                    </t>
                </list>
            </t>
            <t>
                The SF0 allocation policy compares REQUIREDCELLS with SCHEDULEDCELLS and decides to add/delete cells taking into account SF0THRESH.
                This is illustrated in <xref target="fig_allocationpolicy" />.
                The number of cells to be added/deleted is out of the scope of this document and it is implementation-dependent.
            </t>
            <t>
                <figure title="The SF0 Allocation Policy"            anchor="fig_allocationpolicy">
<artwork><![CDATA[
                SCHEDULEDCELLS
    <--------------------------------->
   +---+---+---+---+---+---+---+---+---+
   |   |   |   |   |   |   |   |   |   |
   +---+---+---+---+---+---+---+---+---+
                   |<----------------->|
                   |     SF0THRESH     |
                   |                   |
   REQUIREDCELLS   |                   |
   +---+---+       |                   |       DELETE
   |   |   |       |                   |       ONE/MORE
   +---+---+       |                   |       CELLS
                   |                   |
       REQUIREDCELLS                   |
   +---+---+---+---+---+---+           |       DO
   |   |   |   |   |   |   |           |       NOTHING
   +---+---+---+---+---+---+           |
                   |                   |
               REQUIREDCELLS           |
   +---+---+---+---+---+---+---+---+---+---+   ADD
   |   |   |   |   |   |   |   |   |   |   |   ONE/MORE
   +---+---+---+---+---+---+---+---+---+---+   CELLS
]]></artwork>
                </figure>
            </t>
            <t>
                <list style="numbers">
                    <t>
                        If REQUIREDCELLS&lt;(SCHEDULEDCELLS-SF0THRESH), delete one or more cells.
                    </t>
                    <t>
                        If (SCHEDULEDCELLS-SF0THRESH)&lt;=REQUIREDCELLS&lt;=SCHEDULEDCELLS, do nothing.
                    </t>
                    <t>
                        If SCHEDULEDCELLS&lt;REQUIREDCELLS, add one or more cells.
                    </t>
                </list>
            </t>
            <t>
                When SF0THRESH equals 0, any discrepancy between REQUIREDCELLS and SCHEDULEDCELLS triggers an action to add/delete cells.
                Positive values of SF0THRESH reduce the number of 6P Transactions.
                The number of cells to add or delete is implementation-specific.
            </t>

    </section>

</section>

    <section title="Rules for CellList"                              anchor="sec_celllist">
        <t>
            There are two methods to define the CellList: The Whitelist method, which fills the CellList with the number of proposed cells to the neighbour, and the Blacklist, which fills the CellList with the cells which cannot be used by the neighbour.
            The rule to select the method is implementation-specific.
            When issuing a 6top ADD Request, SF0 executes the following sequence:
            <list style="hyphens">
                <t>
                    Whitelist case:
                    <list style="hyphens">
                        <t>
                            The Transaction Source node: Prepares the CellList field by selecting randomly the required cells, verifying that the slot offset is not occupied and choose channelOffset randomly for each cell.
                        </t>
                        <t>
                            The Transaction Destination node: Goes through the cells in the CellList in order, verifying whether there are no slotOffset conflicts.
                        </t>
                    </list>
                </t>
                <t>
                    Blacklist case:
                    <list style="hyphens">
                        <t>
                            The Transaction Source node: Prepares the CellList field by building a list of currently scheduled cells into the CellList.
                        </t>
                        <t>
                            The Transaction Destination node: Selects randomly the required cells from the unallocated cells on the schedule, verifying that the slot offset is not occupied from the ones on the CellList.
                        </t>
                    </list>
                </t>
            </list>
        </t>
        <t>
            SF0 does not include any transaction retry process.
            If the transaction is not successful, SF0 will be retriggered on the next slotframe if the number of used cells changes.
        </t>
    </section>
    <section title="6P Timeout Value"                                anchor="sec_timeout">
        <t>
            The timeout value is implementation-specific.
            The timeout value MAY be different for each transaction and each neighbour.
            The timeout range is from 0 to 128.
            The timeout MUST be added as an 7-bit on the Metadata header to the neighbour.
            There is no measurement unit associated to the timeout value.
            If the timeout expires, the node issues a RESET return code will be issued to the neighbour.
            SF0 has no retry policy.
            Timeout examples are depicted on <xref target="fig_timeoutnotexpire" /> and <xref target="fig_timeoutexpires" />.
        </t>
            <t>
                <figure title="Example Transaction where the timeout does not expire"            anchor="fig_timeoutnotexpire">
<artwork><![CDATA[
|Timeout Value-----------------------------------------------------|
|A|------First Exchange-------->|B|-----Second Exchange----->|A|
|Complete transaction------------------------------------------|
]]></artwork>
                </figure>
            </t>
            <t>
                <figure title="Example Transaction where the timeout expires"            anchor="fig_timeoutexpires">
<artwork><![CDATA[
|Timeout Value----------------------------------------------|
|A|------First Exchange-------->|B|-----Second Exchange----->|A|
|Non-Complete transaction--------------------------------------|
]]></artwork>
                </figure>
            </t>
    </section>
    <section title="Meaning of Metadata Information"                 anchor="sec_container">
        <t>
            The Metadata 16-bit field is used as follows:
            <list style="hyphens">
                <t>
                    BITS 0-7 [SLOTFRAME] are used to identify the slotframe number
                </t>
                <t>
                    BITS 8-14 [TIMEOUT] represents the Timeout value
                </t>
                <t>
                    BIT 15 [WBLIST] is used to indicate that the CellList provided is a Whitelist (value=0) or a Blacklist (value=1).
                </t>
            </list>
        </t>
    </section>
    <section title="Node Behavior at Boot"                           anchor="sec_boot">
        <t>
            In order to define a known state after the node is restarted, a CLEAR command is issued to each of the neighbor nodes to enable a new allocation process and at least a SF0THRESH number of cells MUST be allocated to each of the neighbours.
        </t>
    </section>
    <section title="Cell Type"                           anchor="sec_celltype">
        <t>
            SF0 uses TX (Transmission) cell type only, thus defining celloptions as TX=0, RX=1 and S=0 according to section 4.2.6 of <xref target="I-D.ietf-6tisch-6top-protocol"/>.
        </t>
    </section>

    <section title="SF0 Statistics"                           anchor="sec_stats">
        <t>
            Packet Delivery Rate (PDR) is calculated per cell, as the percentage of acknowledged packets, for the last 10 packet transmission attempts.
            There is no retransmission policy on SF0.
        </t>
    </section>
    <section title="Relocating Cells"                                anchor="sec_relocation">
        <t>
            Allocated cells may experience packet loss from different sources, such as noise, interference or cell collision (after the same cell is allocated by other nodes in range on the network).
        </t>
        <t>
            SF0 uses Packet Delivery Rate (PDR) statistics to monitor the currently allocated cells for cell relocation (by changing their slotOffset and/or channelOffset).
            When the PDR of one or more softcells is below  PDR_THRESHOLD, SF0 relocates each of the cell(s) to a number of available cells selected randomly.
            PDR_THRESHOLD is out of the scope of this document and it is implementation-dependent.
        </t>
    </section>
    <section title="Forced Cell Deletion Policy"                     anchor="sec_depletion">
        <t>
            When all the cells are scheduled, we need a policy to free cells, for example, under alarm conditions or if a node disappears from the neighbor list.
            The action to follow this condition is out of scope of this document and it is implementation-dependent.
        </t>
    </section>
    <section title="6P Error Handling"                               anchor="sec_error_handling">
        <t>
            A node implementing SF0 handles a 6P Response depending on the Return Code it contains:
        </t>
        <t>
        <list hangIndent="3" style="hanging">
            <t hangText="RC_SUCCESS:">
            </t>
            <t>
                If the number of elements in the CellList is the number of cells specified in the NumCells field of the 6P ADD Request, the operation is complete.
                The node does not take further action.
            </t>
            <t>
                If the number of elements in the CellList is smaller (possibly 0) than the number of cells specified in the NumCells field of the 6P ADD Request, the neighbor has received the request, but less than NumCells of the cells in the CellList were allocated.
                In that case, the node MAY retry immediately with a different CellList if the amount of storage space permits, or build a new (random) CellList.
            </t>
            <t hangText="RC_ERR_VER:">
                The node MUST NOT retry immediately.
                The node MAY add the neighbor node to a blacklist.
                The node MAY retry to contact this neighbor later.
            </t>
            <t hangText="RC_ERR_SFID:">
                The node MUST NOT retry immediately.
                The node MAY add the neighbor node to a blacklist.
                The node MAY retry to contact this neighbor later.
            </t>
            <t hangText="RC_ERR_GEN:">
                The node MUST issue a CLEAR command to the neighbour.
            </t>
            <t hangText="RC_ERR_BUSY:">
                Wait for a timeout and restart the scheduling process.
            </t>
            <t hangText="RC_ERR_NORES:">
                Wait for a timeout and restart the scheduling process.
            </t>
            <t hangText="RC_ERR_RESET:">
                Abort 6P Transaction
            </t>
            <t hangText="RC_ERR:">
                Abort 6P Transaction.
                The node MAY retry to contact this neighbor later.
            </t>
        </list>
        </t>
    </section>
    <section title="Experimental requirements"                                        anchor="sec_experimental">
        
          <t>
          In order to evaluate the performance of this draft, we propose the following experimental work:
          </t>
          <t>  
             <list style="numbers">
                <t>
                   Define values for OVERPROVISION, SF0THRESH and ranges to the number of cells to Add or Delete after the Allocation Policy is applied for typical use cases.
                </t>
                <t>
                   Analyze the scheduling stability (in terms of oscillation) and the hysteresis effect on scheduling using SFX. A tradeoff shall be found between the reactivity of the algorithm facing new scheduling requirements and the number of overprovisioned cells.
                </t>
                <t>
                   Define the PDR value below the Average which is most effective for blacklisting cells and a method to whitelist cells. Analyze the stability and long-term behavior of this algorithm.
                </t>
                <t>
                   Measure the distribution of cell scheduling delay (including the time taken by 6P) to estimate timeouts for different type of transactions
                </t>
            </list>
        </t>
    </section>
    <section title="Examples"                                        anchor="sec_examples">
        <t>
            TODO <!--NICOLA-->
        </t>
    </section>
    <section title="Implementation Status"                           anchor="sec_implementation">
        <!-- START BOILERPLATE FROM RFC6982 -->
        <t>
            This section records the status of known implementations of the protocol defined by this specification at the time of posting of this Internet-Draft, and is based on a proposal described in <xref target="RFC6982" />.
            The description of implementations in this section is intended to assist the IETF in its decision processes in progressing drafts to RFCs.
            Please note that the listing of any individual implementation here does not imply endorsement by the IETF.
            Furthermore, no effort has been spent to verify the information presented here that was supplied by IETF contributors.
            This is not intended as, and must not be construed to be, a catalog of available implementations or their features.
            Readers are advised to note that other implementations may exist.
        </t>
        <t>
            According to <xref target="RFC6982" />, "this will allow reviewers and working groups to assign due consideration to documents that have the benefit of running code, which may serve as evidence of valuable experimentation and feedback that have made the implemented protocols more mature.
            It is up to the individual working groups to use this information as they see fit".
        </t>
        <!-- STOP BOILERPLATE FROM RFC6982 -->
        <t>
            <list style="hanging">
                <t hangText="OpenWSN:">
                    This specification is implemented in the OpenWSN project <xref target="OpenWSN" />.
                    The authors of this document are collaborating with the OpenWSN community to gather feedback about the status and performance of the protocols described in this document.
                    Results from that discussion will appear in this section in future revision of this specification.
                </t>
            </list>
        </t>
    </section>
    <section title="Security Considerations"                         anchor="sec_security">
        <t>
            TODO
        </t>
    </section>
    <section title="IANA Considerations"                             anchor="sec_iana">
        <t>
            <list style="symbols">
                <t>IANA_6TiSCH_SFID_SF0</t>
            </list>
        </t>
    </section>
    <section title="6P Compliance"                             anchor="sec_compliance">
        <t>
            <list style="symbols">
                <t>
                    MUST specify an identifier for that SF.
                    OK
                </t>
                <t>
                    MUST specify the rule for a node to decide when to add/delete one or more cells to a neighbor.
                    OK
                </t>
                <t>
                    MUST specify the rule for a Transaction source to select cells to add to the CellList field in the 6P ADD Request.
                    OK
                </t>
                <t>
                    MUST specify the rule for a Transaction destination to select cells from CellList to add to its schedule.
                    OK
                </t>
                <t>
                    MUST specify a value for the 6P Timeout, or a rule/equation to calculate it.
                    OK
                </t>
                <t>
                    MUST specify a meaning for the "Metadata" field in the 6P ADD Request.
                    OK
                </t>
                <t>
                    MUST specify the behavior of a node when it boots.
                    OK
                </t>
                <t>
                    MUST specify what to do after an error has occurred (either the node sent a 6P Response with an error code, or received one).
                    OK
                </t>
                <t>
                    MUST specify the list of statistics to gather.
                    An example statistic if the number of transmitted frames to each neighbor.
                    In case the SF requires no statistics to be gathered, the specific of the SF MUST explicitly state so.
                    OK
                </t>
                <t>
                    SHOULD clearly state the application domain the SF is created for.
                    OK
                </t>
                <t>
                    SHOULD contain examples which highlight normal and error scenarios.
                </t>
                <t>
                    SHOULD contain a list of current implementations, at least during the I-D state of the document, per [RFC6982].
                </t>
                <t>
                    SHOULD contain a performance evaluation of the scheme, possibly through references to external documents.
                </t>
                <t>
                    MAY redefine the format of the CellList? field.
                    OK
                </t>
            </list>
        </t>
    </section>
    <section title="Acknowledgments"                                 anchor="sec_acks">
        <t>
            Thanks to Kris Pister for his contribution in designing the default Bandwidth Estimation Algorithm.
            Thanks to Qin Wang and Thomas Watteyne for their support in defining the interaction between SF0 and the 6top sublayer.
        </t>
        <t>
            This work is partially supported by the Fondecyt 1121475 Project, the Inria-Chile "Network Design" group, and the IoT6 European Project (STREP) of the 7th Framework Program (Grant 288445).
        </t>
    </section>
</middle>
<back>
    <references title="Normative References">
        <!-- RFC 6TiSCH-->
        <!-- RFC others -->
        <?rfc include='reference.RFC.2119'?>                         <!-- Key words for use in RFCs to Indicate Requirement Levels -->
        <!-- I-D 6TiSCH -->
        <!-- I-D others -->
        <!-- external -->
    </references>
    <references title="Informative References">
       <!-- RFC others -->
        <?rfc include='reference.RFC.6982'?>                         <!-- Improving Awareness of Running Code: The Implementation Status Section -->
        <!-- I-D 6TiSCH -->
        <?rfc include='reference.I-D.ietf-6tisch-terminology'?>
        <?rfc include='reference.I-D.ietf-6tisch-6top-protocol'?>
        <!-- I-D others -->
        <!-- external -->
        <reference                                                   anchor="OpenWSN">
            <front>
                <title>OpenWSN: a Standards-Based Low-Power Wireless Development Environment</title>
                <author initials="T" surname="Watteyne"   fullname="Thomas Watteyne" />
                <author initials="X" surname="Vilajosana" fullname="Xavier Vilajosana" />
                <author initials="B" surname="Kerkez"     fullname="Branko Kerkez" />
                <author initials="F" surname="Chraim"     fullname="Fabien Chraim" />
                <author initials="K" surname="Weekly"     fullname="Kevin Weekly" />
                <author initials="Q" surname="Wang"       fullname="Qin Wang" />
                <author initials="S" surname="Glaser"     fullname="Steven Glaser" />
                <author initials="K" surname="Pister"     fullname="Kris Pister" />
                <date month="August" year="2012" />
            </front>
            <seriesInfo name="Transactions on Emerging Telecommunications Technologies" value="" />
        </reference>
    </references>
    <section title="[TEMPORARY] Changelog">
        <t>
            <list style="symbols">
                <t>draft-ietf-6tisch-6top-sf0-02
                    <list style="symbols">
                        <t>Editorial changes (figs, typos, ...)</t>
                    </list>
                </t>
                <t>draft-ietf-6tisch-6top-sf0-03
                    <list style="symbols">
                        <t>Fixed typos</t>
                        <t>Removed references to "effectively used cells"</t>
                        <t>Changed Cell Estimation Algorithm to the third proposed alternative on IETF97</t>
                        <t>Forced cell deletion becomes implementation specific</t>
                        <t>Added PDR calculation formula</t>
                        <t>Added PDR_THRESHOLD as implementation specific value</t>
                    </list>
                </t>
            </list>
        </t>
    </section>
</back>
</rfc>
